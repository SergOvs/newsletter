package org.ovsiannikov.service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.ovsiannikov.api.SubscriptionService;
import org.ovsiannikov.api.exception.InvalidEmailException;
import org.ovsiannikov.api.model.SubscriptionModel;
import org.ovsiannikov.repository.SubscriptionRepository;

import java.util.List;

public class SubscriptionServiceImplTest {

    private static final String DEFAULT_NAME = "John Smith";
    private static final String DEFAULT_EMAIL = "john@smith.me";

    private SubscriptionRepository repository = new SubscriptionRepository();

    private SubscriptionService service = new SubscriptionServiceImpl(repository);

    @Before
    public void init() {
        repository.afterPropertiesSet();
    }

    @After
    public void cleanup() {
        repository.destroy();
    }

    @Test
    public void saveAndRead() throws InvalidEmailException {
        SubscriptionModel model = new SubscriptionModel();
        model.setName(DEFAULT_NAME);
        model.setEmail(DEFAULT_EMAIL);
        service.save(model);
        List<SubscriptionModel> list = service.getList();

        Assert.assertEquals("must be one object", 1, list.size());
        SubscriptionModel newModel = list.get(0);
        Assert.assertEquals("name as expected", model.getName(), newModel.getName());
        Assert.assertEquals("email as expected", model.getEmail(), newModel.getEmail());
    }

    @Test
    public void wrongEmail() {
        SubscriptionModel model = new SubscriptionModel();
        model.setName(DEFAULT_NAME);
        boolean wasThrown = false;
        try {
            service.save(model);
        } catch (InvalidEmailException e) {
            wasThrown = true;
        }
        Assert.assertTrue("Empty email: exception expected", wasThrown);

        model.setEmail("4@a");
        wasThrown = false;
        try {
            service.save(model);
        } catch (InvalidEmailException e) {
            wasThrown = true;
        }
        Assert.assertTrue("Wrong email: exception expected", wasThrown);
    }

    @Test
    public void listLimit() throws InvalidEmailException {
        SubscriptionModel model = new SubscriptionModel();
        model.setName(DEFAULT_NAME);
        model.setEmail(DEFAULT_EMAIL);
        final int length = 10;
        for (int i = 0; i < length; i++) {
            service.save(model);
        }

        Assert.assertEquals("full set", length, service.getList().size());
        Assert.assertEquals("set of 0", 0, service.getList(0).size());
        Assert.assertEquals("on last page", length % 3, service.getList(length / 3, 3).size());
    }
}