package org.ovsiannikov.service;

import org.ovsiannikov.api.SubscriptionService;
import org.ovsiannikov.api.exception.ActivationCodeCheckFailedException;
import org.ovsiannikov.api.exception.InvalidEmailException;
import org.ovsiannikov.api.exception.ItemNotFoundException;
import org.ovsiannikov.api.model.SubscriptionModel;
import org.ovsiannikov.controller.SubscriptionController;
import org.ovsiannikov.repository.SubscriptionRepository;
import org.ovsiannikov.repository.entity.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Business-layer service, which provides operations with subscriptions
 *
 * @author Ovsiannikov Sergei
 * @since 2019-09-23
 */
@Service
public class SubscriptionServiceImpl implements SubscriptionService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionController.class);

    @SuppressWarnings("RegExpRedundantEscape")
    private static final String VALID_EMAIL_REGEXP = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+$";
    private static final int DEFAULT_LIMIT = 100;

    private final SubscriptionRepository subscriptionRepository;

    @Autowired
    public SubscriptionServiceImpl(SubscriptionRepository subscriptionRepository) {
        this.subscriptionRepository = subscriptionRepository;
    }

    @Override
    public List<SubscriptionModel> getList() {
        return getList(DEFAULT_LIMIT);
    }

    @Override
    public List<SubscriptionModel> getList(int limit) {
        return getList(0, limit);
    }

    @Override
    public List<SubscriptionModel> getList(int pageNum, int pageSize) {
        return subscriptionRepository.getList(pageNum, pageSize)
                .stream()
                .map(ModelConverterUtils::convertToLogical)
                .collect(Collectors.toList());
    }

    @Override
    public void save(SubscriptionModel subscription) throws InvalidEmailException {
        validateEmail(subscription.getEmail());

        Subscription entity = ModelConverterUtils.convertToPhysical(subscription);
        subscriptionRepository.save(entity);

        if (LOGGER.isInfoEnabled()) {
            try {
                LOGGER.info("new object's activation code: {}", calculateMD5HashString(subscription.getEmail()));
            } catch (NoSuchAlgorithmException e) {
                LOGGER.error("calculation error: {}", e.getMessage(), e);
            }
        }
    }

    @Override
    public void confirm(Long subscriptionId, String activationCode) throws ItemNotFoundException, ActivationCodeCheckFailedException {
        Subscription subscription = subscriptionRepository.getSubscription(subscriptionId);
        if (subscription == null) {
            throw new ItemNotFoundException("There in no subscription found with id = " + subscriptionId);
        }
        try {
            String hashtext = calculateMD5HashString(subscription.getEmail());
            LOGGER.debug("calculated code: {}, received: {}", hashtext, activationCode);
            if (!hashtext.equals(activationCode)) {
                throw new ActivationCodeCheckFailedException("wrong activation code: " + activationCode);
            }
        } catch (NoSuchAlgorithmException e) {
            throw new ActivationCodeCheckFailedException("error while checking activation code: " + e.getMessage(), e);
        }
        subscription.setConfirmed(true);
        subscriptionRepository.save(subscription);
        LOGGER.info("activation successful: {}", subscription.getId());
    }

    private void validateEmail(String email) throws InvalidEmailException {
        if (email == null || !email.matches(VALID_EMAIL_REGEXP)) {
            throw new InvalidEmailException("invalid email: " + email);
        }
    }

    private String calculateMD5HashString(String email) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] digest = md.digest(email.getBytes());
        BigInteger bigInt = new BigInteger(1, digest);
        return bigInt.toString(16);
    }
}
