package org.ovsiannikov.service;

import org.ovsiannikov.api.model.SubscriptionModel;
import org.ovsiannikov.repository.entity.Subscription;

/**
 * Converter between physical and logical data model
 *
 * @author Ovsiannikov Sergei
 * @since 2019-09-23
 */
class ModelConverterUtils {

    /**
     * We do not need to save instance of this utility
     */
    private ModelConverterUtils(){
        // prohibited
    }

    /**
     * Convert Physical -> Logical
     *
     * @param subscription  DAO entity instance
     * @return business logic instance
     */
    static SubscriptionModel convertToLogical(Subscription subscription) {
        SubscriptionModel model = new SubscriptionModel();
        model.setId(subscription.getId());
        model.setName(subscription.getName());
        model.setEmail(subscription.getEmail());
        return model;
    }

    /**
     * Convert Logical -> Physical
     **
     * @param model business logic instance
     * @return DAO entity
     */
    static Subscription convertToPhysical(SubscriptionModel model) {
        Subscription subscription = new Subscription();
        subscription.setId(model.getId());
        subscription.setName(model.getName());
        subscription.setEmail(model.getEmail());
        return subscription;
    }
}
