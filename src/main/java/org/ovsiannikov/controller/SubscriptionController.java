package org.ovsiannikov.controller;

import org.ovsiannikov.api.SubscriptionService;
import org.ovsiannikov.api.exception.ActivationCodeCheckFailedException;
import org.ovsiannikov.api.exception.ItemNotFoundException;
import org.ovsiannikov.api.model.SubscriptionModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller for email subscription operations
 *
 * @author Ovsiannikov Sergei
 * @since 2019-09-23
 */
@Controller
public class SubscriptionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionController.class);

    private final SubscriptionService subscriptionService;

    @Autowired
    public SubscriptionController(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    /**
     * Prepare list of subscribers to display in view
     *
     * @param model spring model
     */
    @GetMapping("/list")
    public String list(Model model) {
        List<SubscriptionModel> list = subscriptionService.getList();
        model.addAttribute("subscribers", list);
        return "list";
    }

    /**
     * Simple request form
     */
    @GetMapping("/form")
    public String save() {
        return "form";
    }

    /**
     * Submit subscription to processing
     *
     * @param subscription Business model of subscription (DTO)
     */
    @PostMapping("/save")
    public String save(@ModelAttribute("subscription") SubscriptionModel subscription, Model model) {
        try {
            LOGGER.trace("save request: {}", subscription);
            subscriptionService.save(subscription);
        } catch (Exception e) {
            LOGGER.error("Error while saving subscription: {}", subscription, e);
            model.addAttribute("error", e.getMessage());
            return "form";
        }
        return "redirect:/";
    }

    /**
     * Activate subscription by activation code
     *
     * @param id subscription identifier
     * @param activationCode activation code
     */
    @GetMapping("/activate")
    public String activate(@RequestParam Long id, @RequestParam String activationCode) {
        try {
            LOGGER.trace("activate request: id=<{}>, activation=<{}>", id, activationCode);
            subscriptionService.confirm(id, activationCode);
        } catch (ActivationCodeCheckFailedException | ItemNotFoundException e) {
            LOGGER.error("Error while activate subscription: id=<{}>, activation=<{}>", id, activationCode, e);
        }
        return "redirect:/";
    }
}
