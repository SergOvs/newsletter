package org.ovsiannikov.repository;

import org.ovsiannikov.repository.entity.Subscription;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.List;

/**
 * DAO to store subscription data
 *
 * @author Ovsiannikov Sergei
 * @since 2019-09-23
 */
@Repository
public class SubscriptionRepository implements InitializingBean, DisposableBean {

    private EntityManagerFactory entityManagerFactory;

    public Subscription save(Subscription subscription) {
        EntityManager entityManager = getEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        Subscription newSubscription = entityManager.merge(subscription);
        transaction.commit();
        entityManager.close();
        return newSubscription;
    }

    public List<Subscription> getList(int pageNum, int pageSize) {
        EntityManager entityManager = getEntityManager();
        List<Subscription> list;
        Query query = entityManager.createQuery("from Subscription order by id");
        query.setFirstResult(pageNum * pageSize);
        query.setMaxResults(pageSize);
        //noinspection unchecked
        list = query.getResultList();
        entityManager.close();
        return list;
    }

    public Subscription getSubscription(Long subscriptionId) {
        EntityManager entityManager = getEntityManager();
        Query query = entityManager.createQuery("from Subscription where id = :id");
        query.setParameter("id", subscriptionId);
        Subscription subscription = (Subscription) query.getSingleResult();
        entityManager.close();
        return subscription;
    }

    @Override
    public void afterPropertiesSet() {
        entityManagerFactory = Persistence.createEntityManagerFactory("h2_persistence_unit");
    }

    @Override
    public void destroy() {
        entityManagerFactory.close();
    }

    private EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

}
