package org.ovsiannikov.api.exception;

public class ActivationCodeCheckFailedException extends Exception {

    public ActivationCodeCheckFailedException(String message) {
        super(message);
    }

    public ActivationCodeCheckFailedException(String message, Throwable cause) {
        super(message, cause);
    }
}
