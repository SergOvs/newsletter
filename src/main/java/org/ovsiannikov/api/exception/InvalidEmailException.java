package org.ovsiannikov.api.exception;

/**
 * Can be thrown if submitted email address is invalid
 *
 * @author Ovsiannikov Sergei
 * @since 2019-09-23
 */
public class InvalidEmailException extends Exception {

    public InvalidEmailException(String s) {
        super(s);
    }

    public InvalidEmailException(String s, Throwable e) {
        super(s, e);
    }
}
