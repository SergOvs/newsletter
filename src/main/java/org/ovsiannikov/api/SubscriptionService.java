package org.ovsiannikov.api;

import org.ovsiannikov.api.exception.ActivationCodeCheckFailedException;
import org.ovsiannikov.api.exception.InvalidEmailException;
import org.ovsiannikov.api.exception.ItemNotFoundException;
import org.ovsiannikov.api.model.SubscriptionModel;

import java.util.List;

/**
 * Interface for basic service operations with subscriptions
 *
 * @author Ovsiannikov Sergei
 * @since 2019-09-23
 */
public interface SubscriptionService {

    /**
     * Get list of subscription
     *
     * @return list of subscription
     */
    List<SubscriptionModel> getList();

    List<SubscriptionModel> getList(int limit);

    List<SubscriptionModel> getList(int pageNum, int pageSize);

    /**
     * Apply new subscription
     *
     * @param subscription new subscription
     * @throws InvalidEmailException if email address is malformed
     */
    void save(SubscriptionModel subscription) throws InvalidEmailException;

    /**
     * This method marks the subscription as valid
     *
     * @param subscriptionId id of stored subscription
     * @param activationCode activation code
     */
    void confirm(Long subscriptionId, String activationCode) throws ActivationCodeCheckFailedException, ItemNotFoundException;
}
