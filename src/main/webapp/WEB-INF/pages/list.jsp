<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>list</title>
</head>
<body>

<a href="${pageContext.request.contextPath}/">&lt; back</a>

<h2>List of subscribers</h2>

<table>
    <tr>
        <th>#</th>
        <th>name</th>
        <th>email</th>
    </tr>

    <c:forEach items="${subscribers}" var="item">
        <tr>
            <td>${item.id}</td>
            <td>${item.name}</td>
            <td>${item.email}</td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
