<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>new</title>
</head>
<body>

<a href="${pageContext.request.contextPath}/">&lt; back</a>

<h2>New subscription</h2>

<form:form action="${pageContext.request.contextPath}/save" method="post">
    <label for="name">name:</label>
    <input name="name" id="name" value="${subscription.name}"/><br />
    <label for="email">email:</label>
    <input name="email" id="email" value="${subscription.email}"/><br />
    <input type="submit" />
</form:form>

<span style="color: red">${error}</span>

</body>
</html>
